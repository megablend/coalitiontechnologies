@extends('layouts.layout')
@section('title', $title)

@section('content')
<div class="row">
	<div class="col-md-12">

		 <form no-validate data-ng-submit="submitStock()" name="stock.form" id="stock-form">
		  <fieldset class="form-group">
		    <label for="exampleInputEmail1">Product Name</label>
		    <input type="text" class="form-control" name="product_name" data-ng-init="stock.product_name = '{{ old('product_name') }}'" data-ng-model="stock.product_name" id="exampleInputEmail1" placeholder="Enter Product Name" required>
		    <small class="text-danger text-muted" data-ng-show="stock.form.product_name.$error.required && stock.form.product_name.$dirty">Please enter the product name.</small>
		  </fieldset>

		  <fieldset class="form-group">
		    <label for="exampleInputEmail1">Quantity</label>
		    <input type="number" class="form-control" name="quantity" data-ng-init="stock.quantity='{{ old('quantity') }}'" data-ng-model="stock.quantity" id="exampleInputEmail1" placeholder="Enter Quantity" required>
		    <small class="text-danger text-muted" data-ng-show="stock.form.quantity.$error.required && stock.form.quantity.$dirty">Please enter the quantity.</small>
		  </fieldset>

		  <fieldset class="form-group">
		    <label for="exampleInputEmail1">Price</label>
		    <input type="number" class="form-control" name="price" data-ng-init="stock.price='{{ old('price') }}'" data-ng-model="stock.price" id="exampleInputEmail1" placeholder="Enter Price" required>
		    <small class="text-danger text-muted" data-ng-show="stock.form.price.$error.required && stock.form.price.$dirty">Please enter the price.</small>
		  </fieldset>
		  <button type="submit" id="submit-btn" class="btn btn-primary" data-ng-disabled="stock.form.$invalid">Submit</button>
		</form>

		<table class="table" data-ng-show="stocks.length"> 
			<thead class="thead-inverse"> 
				<tr> 
					<th>#</th> 
					<th>Product Name</th>
					 <th>Quantity in Stock</th> 
					 <th>Price Per Item</th> 
					 <th>Datetime Submitted</th> 
					 <th>Total Value</th> 
				</tr> 
			</thead> 
			<tbody> 
				<tr data-ng-repeat="stock in stocks | orderBy:'-created_at'"> 
					<th scope="row">@{{ $index + 1 }}</th> 
					<td>@{{ stock.product_name }}</td> 
					<td>@{{ stock.quantity }}</td> 
					<td>@{{ stock.price | currency }}</td> 
					<td>@{{ stock.created_at }}</td> 
					<td>@{{ stock.price * stock.quantity | currency }}</td> 
				</tr> 
			</tbody> 
		</table> 
  </div>
</div>
@endsection