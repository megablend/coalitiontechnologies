<!DOCTYPE html>
<html lang="en">
<head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Stylesheet -->
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,100,400italic,500,300italic,500italic,700,700italic' rel='stylesheet' type='text/css'>
        <link href="{{ URL::asset('public/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('public/assets/css/style.css') }}" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('public/assets/css/sweetalert.css') }}">
        <meta name="application-name" content="Coalition Technologies"/>

        <!-- Title -->
        <title>{{ $app_name }} - @yield('title')</title>
        
        <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
        <script src="{{ URL::asset('public/assets/js/jquery-1.12.3.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('public/assets/js/angular/angular.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('public/assets/js/angular/app.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('public/assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('public/assets/js/sweetalert.min.js') }}"></script>
    </head>

    <body data-ng-app="ctApp" data-ng-controller="CtController">
        <div class="page-header">
          <h1>Coalition Technologies Application</h1>
        </div>
       <div class="container">
           <!-- Content
        ==================================-->
        @yield('content')
       </div>

    </body>
</html>