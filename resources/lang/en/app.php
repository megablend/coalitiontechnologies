<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'stock_form_error' => 'Errors in the submitted form, please check the form.',
    'stock_form_successful' => 'Stock was successfully added',

];
