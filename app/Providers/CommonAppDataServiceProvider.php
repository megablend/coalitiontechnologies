<?php

namespace App\Providers;

use App\State;
use Illuminate\Support\ServiceProvider;

class CommonAppDataServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->share('app_name', 'Coalition App');
        view()->share('company_telephone', '0000000000');
        view()->share('company_address', 'No where');
        view()->share('company_name', 'Nexus Axis Projects Limited');
        // view()->share('states', State::all());
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
