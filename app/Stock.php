<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
	/**
	 * The stocks table
	 * 
	 * @var string
	 */
    protected $table = "stocks";
    protected $dates = ['created_at', 'updated_at'];
}
