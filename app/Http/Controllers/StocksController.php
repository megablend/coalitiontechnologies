<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Stock;
use Validator;
use Storage;

class StocksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = "Home Page";
        return view('index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $params = $request->input('params');
        $validator = Validator::make($params, ['product_name' => 'required',
                                               'quantity' => 'required|numeric',
                                               'price' => 'required|numeric']);
        if($validator->fails()){
            $data['processed'] = false;
            $data['errors'] = $validator->errors()->toJson();
            $data['feedback'] = trans('app.stock_form_error');
        }
        else{
            $stock = new Stock;
            $stock->product_name = $params['product_name'];
            $stock->quantity = $params['quantity'];
            $stock->price = $params['price'];

            //save data
            $stock->save();

            $stocks = Stock::all();

            //create a json file
            Storage::put('stocks.json', response()->json($stocks));

            $data['processed'] = true;
            $data['data'] = $stock;
            $data['feedback'] = trans('app.stock_form_successful');
        }
        $data['processed'] = $request->all();
        return response()->json($data);
    }

    /**
     * Get all the stocks
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request)
    {
        $stocks = Stock::all();
        if($stocks->count()){
            $data['processed'] = true;
            $data['data'] = $stocks;
        }
        else{
            $data['processed'] = false;
        }
        return response()->json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
