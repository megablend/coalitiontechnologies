(function(){
  var prefixUrl = 'coalitiontechnologies/';
  var app = angular.module('ctApp', []);


  /**
   * General Controller
   * 
   */
  app.controller('CtController', ['$scope', 'defaultService', '$timeout', function($scope, defaultService, $timeout){
    $scope.stocks = [];

    //get all stocks
     url = '/' + prefixUrl + '/get-stocks';
     defaultService.allGetRequests(url)
     .then(function(resp){
         if(resp.processed){
            angular.forEach(resp.data, function(obj){
               $scope.stocks.push(obj);
            });
         }
         console.log(angular.toJson($scope.stocks));
     })
     .then(function(err){
        if(typeof err != 'undefined') console.log(err);
     });


    $scope.submitStock = function(){
         $("#submit-btn").html('Please wait...');
         $timeout(function(){
             url = '/' + prefixUrl + '/';
             params = { product_name : $scope.stock.product_name,
                        quantity : $scope.stock.quantity,
                        price : $scope.stock.price };
             defaultService.allPostRequests(url, params)
             .then(function(resp){
                 $("#submit-btn").html('Submit');
                 console.log(angular.toJson(resp));
                 if(resp.processed){
                    var stock = {};
                    stock.id = resp.data.id;
                    stock.product_name = resp.data.product_name;
                    stock.quantity = resp.data.quantity;
                    stock.price = resp.data.price;
                    stock.created_at = resp.data.created_at;
                    stock.updated_at = resp.data.updated_at;
                    $scope.stocks.push(stock);
                    
                    swal('Stock', resp.feedback, 'success');
                    return;
                 }
                 else{
                    swal('Stock Error', resp.feedback, 'error');
                    return;
                 }
             })
             .then(function(err){
                if(typeof err != 'undefined') console.log(err);
             });
         }, 2000);
    }
  }]);

  /**
   * Default Services
   * 
   */
  
  /** 
    * Default Service
    *
    */
    app.factory('defaultService', ['$http', '$q', function($http, $q){
       return {
           validateUserEmail: function(email){
            var defer = $q.defer();
            var promise = defer.promise;
            var params = { email: email };
            $http.post('check-user-email', { params: params }).success(function(response){
              defer.resolve(response);
            })
            .error(function(error){
                  defer.resolve(error);
            });
              //return promise object
            return promise;
           },
           allPostRequests : function(url, params){
                var defer = $q.defer();
                var promise = defer.promise;
                $http.post(url, { params: params }).success(function(response){
                  defer.resolve(response);
                })
                .error(function(error){
                      defer.resolve(error);
                });
                  //return promise object
                return promise;
           },
           allGetRequests : function(url){
              var defer = $q.defer();
              var promise = defer.promise;
              $http.get(url).success(function(response){
                 defer.resolve(response);
              })
              .error(function(error){
                defer.resolve(error)
              });

              //return promise
              return promise;
           },
           allPutRequests : function(url, params){
              var defer = $q.defer();
              var promise = defer.promise;
              $http.post(url, { params: params }).success(function(response){
                defer.resolve(response);
              })
              .error(function(error){
                    defer.resolve(error);
              });
                //return promise object
              return promise;
         },
         allDeleteRequests : function(url, params){
            var defer = $q.defer();
            var promise = defer.promise;
            $http.delete(url, { params : params }).success(function(response){
                defer.resolve(response);
            })
            .error(function(error){
               defer.resolve(error);
            });

            //return promise object
            return promise;
         }
       };
    }]);

}());